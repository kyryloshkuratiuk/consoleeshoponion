﻿using System;
using Domain.Core;
using Infrastructure.Data;
using Infrastructure.Data.OrderService;
using Infrastructure.Data.ProductService;
using Infrastructure.Data.RegisteredAccountService;
using Presentation.UI;

namespace ConsoleEShop_Onion
{
    class Program
    {
        static void Main(string[] args)
        {
            User currentUser = null;
            RegisteredAccountRepository userDataBase = new RegisteredAccountRepository();
            OrderRepository orderDataBase = new OrderRepository();
            ProductRepository productDataBase = new ProductRepository();
            Admin admin = new Admin("kirill", "6813261", "kirillshkuratiuk@gmail.com"); // Adding elements to the imaginary database
            User user = new User("pitbull", "1337228", "iloveyou@and.me");
            productDataBase.Add(4, "Samsung Galaxy S8 64GB", "Phones", "Smartphone", 14999);
            productDataBase.Add(5, "Samsung Note 9 128GB", "Phones", "Smartphone", 34999);
            productDataBase.Add(6, "Apple Watch Series 5 44mm", "Watches", "Smartwatch", 14999);
            userDataBase.Add(admin);
            userDataBase.Add(user);
            Menu menu = new Menu(currentUser, userDataBase, orderDataBase, productDataBase);
            menu.Start();

        }
    }
}
