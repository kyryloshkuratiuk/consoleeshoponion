using NUnit.Framework;
using Domain.Core;
using Infrastructure.Data.ProductService;
using Infrastructure.Data.OrderService;
using Infrastructure.Data.RegisteredAccountService;
using System.Collections.Generic;
using Infrastructure.Business.UserBusiness;
using System.Linq;
using System;
using Infrastructure.Data.GuestService;

namespace ConsoleEShop_Onion.Tests
{
    public class BusinessTests
    {
        private ProductRepository productService;
        private OrderRepository orderService;
        private RegisteredAccountRepository userService;
        public UserInfoRepository userInfoService;
        public GuestRepository guestService = new GuestRepository();

        [TestCase(1, "IPhone 11 Pro Max 64GB", "Phones", "Smartphone", 34999)]
        public void Add_AddNotEmptyProduct_ReturnsCorrectLength(int id, string name, string category, string description, decimal price)
        {
            // Arrange
            productService = new ProductRepository();
            int expectedLength = 1;
            productService.Add(id, name, category, description, price);

            // Act
            var actualLength = productService.GetProducts().Count;

            // Assert
            Assert.AreEqual(expectedLength, actualLength);
        }

        [TestCase(1, "IPhone 11 Pro Max 64GB", "Phones", "Smartphone", 34999)]
        public void Remove_RemoveNotEmptyProduct_ReturnsCorrectLength(int id, string name, string category, string description, decimal price)
        {
            // Arrange
            productService = new ProductRepository();
            int expectedLength = 0;
            productService.Add(id, name, category, description, price);
            productService.Remove(id);

            // Act
            var actualLength = productService.GetProducts().Count;

            // Assert
            Assert.AreEqual(expectedLength, actualLength);
        }

        [TestCase(1, "IPhone 11 Pro Max 64GB", "Phones", "Smartphone", 34999)]
        public void Find_FindExistingProduct_ReturnsListWithFoundProduct(int id, string name, string category, string description, decimal price)
        {
            // Arrange
            productService = new ProductRepository();
            productService.Add(id, name, category, description, price);
            List<Product> expectedList = new List<Product>();
            expectedList.Add(productService.GetProductById(id));

            // Act
            var actualList = productService.Find(name);


            // Assert
            CollectionAssert.AreEqual(expectedList, actualList);
        }

        [Test]
        public void Find_FindNotExistingProduct_ReturnsEmptyList()
        {
            // Arrange
            productService = new ProductRepository();
            List<Product> expected = new List<Product>();

            // Act
            var actual = productService.Find("NotExistingProduct");

            // Assert
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestCase(1, "IPhone 11 Pro Max 64GB", "Phones", "Smartphone", 34999)]
        public void GetProducts_ReturnsListOfExistingProducts(int id, string name, string category, string description, decimal price)
        {
            // Arrange
            productService = new ProductRepository();
            List<Product> expected = new List<Product>();
            productService.Add(id, name, category, description, price);
            expected.Add(productService.GetProductById(id));

            // Act
            List<Product> actual = productService.GetProducts();

            // Assert
            CollectionAssert.AreEqual(expected, actual);
        }

        [Test, TestCaseSource("NotEmptyProduct")]
        public void Create_CreateNotEmptyOrder_ReturnsCorrectId(Product product)
        {
            // Arrange
            orderService = new OrderRepository();
            int expectedId = 0;

            // Act
            var actualId = orderService.Create();

            // Assert
            Assert.AreEqual(expectedId, actualId);
        }

        [Test, TestCaseSource("NotEmptyProduct")]
        public void Delete_DeleteNotEmptyOrder_ReturnsCorrectStatus(Product product)
        {
            // Arrange
            orderService = new OrderRepository();
            int order = orderService.Create();
            orderService.Delete(order);
            string expectedStatus = "Canceled by user";

            // Act
            string actualStatus = orderService.GetOrderById(order).Status;

            // Assert
            Assert.AreEqual(expectedStatus, actualStatus);
        }

        [Test, TestCaseSource("NotEmptyProduct")]
        public void GetOrderById__ReturnsOrderId(Product product)
        {
            // Arrange
            orderService = new OrderRepository();
            int expectedId = orderService.Create();

            // Act
            var actualId = orderService.GetOrderById(0).Id;

            // Assert
            Assert.AreEqual(expectedId, actualId);
        }

        [Test, TestCaseSource("NotEmptyUser")]
        public void Add_AddNotEmptyUser_ReturnsCorrectLength(User user)
        {
            // Arrange
            userService = new RegisteredAccountRepository();
            int expectedLength = 1;
            userService.Add(user);

            // Act
            var actualLength = userService.GetUsers().Count;

            // Assert
            Assert.AreEqual(expectedLength, actualLength);
        }

        [Test, TestCaseSource("NotEmptyUser")]
        public void Remove_RemoveNotEmptyUser_ReturnsCorrectLength(User user)
        {
            // Arrange
            userService = new RegisteredAccountRepository();
            int expectedLength = 0;
            userService.Add(user);
            userService.Remove(user);

            // Act
            var actualLength = userService.GetUsers().Count;

            // Assert
            Assert.AreEqual(expectedLength, actualLength);
        }

        [Test, TestCaseSource("NotEmptyUser")]
        public void ContainsEmail_DataBaseContainsEmail_ReturnsTrue(User user)
        {
            // Arrange
            userService = new RegisteredAccountRepository();
            userService.Add(user);
            bool expectedBoolean = true;

            // Act
            bool actualBoolean = userService.ContainsEmail(user.Email);

            // Assert
            Assert.AreEqual(expectedBoolean, actualBoolean);
        }

        [Test, TestCaseSource("NotEmptyUser")]
        public void ContainsEmail_DataBaseDoesNotContainsEmail_ReturnsFalse(User user)
        {
            // Arrange
            userService = new RegisteredAccountRepository();
            userService.Add(user);
            bool expectedBoolean = false;

            // Act
            bool actualBoolean = userService.ContainsEmail(string.Empty);

            // Assert
            Assert.AreEqual(expectedBoolean, actualBoolean);
        }

        [Test, TestCaseSource("NotEmptyUser")]
        public void ContainsLogin_DataBaseContainsLogin_ReturnsTrue(User user)
        {
            // Arrange
            userService = new RegisteredAccountRepository();
            userService.Add(user);
            bool expectedBoolean = true;

            // Act
            bool actualBoolean = userService.ContainsLogin(user.Login);

            // Assert
            Assert.AreEqual(expectedBoolean, actualBoolean);
        }

        [Test, TestCaseSource("NotEmptyUser")]
        public void ContainsLogin_DataBaseDoesNotContainsLogin_ReturnsFalse(User user)
        {
            // Arrange
            userService = new RegisteredAccountRepository();
            userService.Add(user);
            bool expectedBoolean = false;

            // Act
            bool actualBoolean = userService.ContainsLogin(string.Empty);

            // Assert
            Assert.AreEqual(expectedBoolean, actualBoolean);
        }

        [Test, TestCaseSource("NotEmptyUser")]
        public void GetUser_DataBaseContainsUser_ReturnsUser(RegisteredAccount user)
        {
            // Arrange
            userService = new RegisteredAccountRepository();
            userService.Add(user);
            RegisteredAccount expectedUser = user;

            // Act
            RegisteredAccount actualUser = userService.GetUser(user.Login, user.Password);

            // Assert
            Assert.AreEqual(expectedUser, actualUser);
        }

        [Test, TestCaseSource("NotEmptyUser")]
        public void GetUser_DataBaseDoesNotContainsUser_ReturnsUser(User user)
        {
            // Arrange
            userService = new RegisteredAccountRepository();
            userService.Add(user);
            var expectedException = typeof(System.ArgumentException);

            // Act
            var actualException = Assert.Catch(() => userService.GetUser(string.Empty, string.Empty));

            // Assert
            Assert.AreEqual(expectedException, actualException.GetType());
        }

        [Test, TestCaseSource("NotEmptyUser")]
        public void GetUsers_ReturnsListOfExistingUsers(RegisteredAccount user)
        {
            // Arrange
            userService = new RegisteredAccountRepository();
            List<RegisteredAccount> expected = new List<RegisteredAccount>();
            userService.Add(user);
            expected.Add(userService.GetUser(user.Login, user.Password));

            // Act
            List<RegisteredAccount> actual = userService.GetUsers();

            // Assert
            CollectionAssert.AreEqual(expected, actual);
        }

        [Test, TestCaseSource("NotEmptyUser")]
        public void SetLogin_ChangeUserLogin_ChangesLoginSuccesfully(User user)
        {
            // Arrange
            userService = new RegisteredAccountRepository();
            userInfoService = new UserInfoRepository(user, userService);
            userService.Add(user);
            string expectedLogin = "newLogin";
            userInfoService.ChangeLogin("newLogin");

            // Act
            string actualLogin = user.Login;

            // Assert
            Assert.AreEqual(expectedLogin, actualLogin);
        }

        [Test, TestCaseSource("NotEmptyUser")]
        public void SetLogin_ChangeUserLogin_ThrowsArgumentException(User user)
        {
            // Arrange
            userService = new RegisteredAccountRepository();
            userInfoService = new UserInfoRepository(user, userService);
            userService.Add(user);
            var expectedException = typeof(ArgumentException);

            // Act
            var actualException = Assert.Catch(() => userInfoService.ChangeLogin(user.Login));

            // Assert
            Assert.AreEqual(expectedException, actualException.GetType());
        }

        [Test, TestCaseSource("NotEmptyUser")]
        public void SetEmail_ChangeUserEmail_ChangesEmailSuccesfully(User user)
        {
            // Arrange
            userService = new RegisteredAccountRepository();
            userInfoService = new UserInfoRepository(user, userService);
            userService.Add(user);
            string expectedEmail = "newEmail@mail.com";
            userInfoService.ChangeEmail("newEmail@mail.com");

            // Act
            string actualEmail = user.Email;

            // Assert
            Assert.AreEqual(expectedEmail, actualEmail);
        }

        [Test, TestCaseSource("NotEmptyUser")]
        public void SetEmail_ChangeUserEmail_ThrowsArgumentException(User user)
        {
            // Arrange
            userService = new RegisteredAccountRepository();
            userInfoService = new UserInfoRepository(user, userService);
            userService.Add(user);
            var expectedException = typeof(ArgumentException);

            // Act
            var actualException = Assert.Catch(() => userInfoService.ChangeEmail("?"));

            // Assert
            Assert.AreEqual(expectedException, actualException.GetType());
        }

        [Test, TestCaseSource("NotEmptyUser")]
        public void SetPassword_ChangeUserPassword_ChangesPasswordSuccesfully(User user)
        {
            // Arrange
            userService = new RegisteredAccountRepository();
            userService.Add(user);
            string expectedPassword = "<epam>";
            user.SetPassword("<epam>");

            // Act
            string actualPassword = user.Password;

            // Assert
            Assert.AreEqual(expectedPassword, actualPassword);
        }

        [Test, TestCaseSource("NotEmptyOrder")]
        public void AddOrder_AddsOrderToUserOrders_OrderAddedSuccesfully(Order order)
        {
            // Arrange
            User user = new User("Kirill", "12345", "kirill@gmail.com");
            userService = new RegisteredAccountRepository();
            userService.Add(user);
            user.AddOrder(order);
            bool expectedBoolean = true;

            // Act
            bool actualBoolean = user.Orders.Contains(order);

            // Assert
            Assert.AreEqual(expectedBoolean, actualBoolean);
        }

        [TestCase("valid", "Unique", "user@gmail.com")]
        public void Register_RegisterNewValidUser_ReturnsTrue(string login, string password, string email)
        {
            // Arrange
            userService = new RegisteredAccountRepository();
            User expectedUser = new User(login, password, email);
            string expectedLogin = expectedUser.Login;
            guestService.Register(login, password, email, userService);

            // Act
            RegisteredAccount actualUser = userService.GetUser(login, password);
            string actualLogin = actualUser.Login;


            // Assert
            Assert.AreEqual(expectedLogin, actualLogin);
        }

        [TestCase("?", "12345", "kirill@gmail.com")]
        public void Register_UserWithInvalidInputData_ThrowsArgumentException(string login, string password, string email)
        {
            // Arrange
            userService = new RegisteredAccountRepository();
            var expectedException = typeof(ArgumentException);

            // Act
            var actualException = Assert.Catch(() => guestService.Register(login, password, email, userService));

            // Assert
            Assert.AreEqual(expectedException, actualException.GetType());
        }

        [TestCase("?", "12345", "??")]
        public void Authorize_AuthorizeWithValidData_ReturnsTrue(string login, string password, string email)
        {
            // Arrange
            userService = new RegisteredAccountRepository();
            RegisteredAccount expectedUser = new User(login, password, email);
            string expectedLogin = expectedUser.Login;
            userService.Add(expectedUser);

            // Act
            RegisteredAccount actualUser = guestService.Authorize(login, password, userService);
            string actualLogin = actualUser.Login;


            // Assert
            Assert.AreEqual(expectedLogin, actualLogin);
        }

        [TestCase("Kirill", "12345", "kirill@gmail.com")]
        public void Authorize_AuthorizeWithInvalidData_ThrowsArgumentException(string login, string password, string email)
        {
            // Arrange
            User user = new User(login, password, email);
            userService.Add(user);
            var expectedException = typeof(ArgumentException);

            // Act
            var actualException = Assert.Catch(() => guestService.Authorize("login", "password", userService));

            // Assert
            Assert.AreEqual(expectedException, actualException.GetType());
        }


        static Product[] NotEmptyProduct =
        {
            new Product(1, "IPhone 11 Pro Max 64GB", "Phones", "Smartphone", 34999)
        };

        static Product[][] NotEmptyProductArray =
        {
            new Product[]
            {
                new Product(4, "Samsung Galaxy S8 64GB", "Phones", "Smartphone", 14999),
                new Product(5, "Samsung Note 9 128GB", "Phones", "Smartphone", 34999),
                new Product(6, "Apple Watch Series 5 44mm", "Watches", "Smartwatch", 14999)
            }
        };

        static Order[] NotEmptyOrder =
        {
            new Order(0, NotEmptyProductArray[0].ToList())
        };

        static Order[][] NotEmptyOrderArray =
        {
            new Order[]
            {
                new Order(0, NotEmptyProductArray[0].ToList()),
                new Order(0, NotEmptyProductArray[0].ToList()),
                new Order(0, NotEmptyProductArray[0].ToList())
            }
        };



        static User[] NotEmptyUser =
        {
            new User("Kirill", "12345", "kirill@gmail.com")
        };

    }
}