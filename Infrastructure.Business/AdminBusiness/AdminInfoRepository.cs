﻿using System;
using System.Text.RegularExpressions;
using Domain.Core;
using Infrastructure.Data.RegisteredAccountService;

namespace Infrastructure.Business.AdminBusiness
{
    public class AdminInfoRepository
    {
        readonly RegisteredAccountRepository userDataBase;

        public AdminInfoRepository(RegisteredAccountRepository userService)
        {
            userDataBase = userService;
        }

        /// <summary>
        /// Gets personal data of provided user
        /// </summary>
        /// <param name="login">Login of needed user</param>
        /// <exception cref="ArgumentException">Thrown if user not found</exception>
        public string GetPersonalData(string login)
        {
            User user = userDataBase.GetUserByLogin(login) as User;
            if (user == null) throw new ArgumentException("User not found");
            return user.ToString();
        }

        /// <summary>
        /// Changes the login of provided user
        /// </summary>
        /// <param name="login">Login of needed user</param>
        /// <param name="newLogin">New login for user</param>
        /// <exception cref="ArgumentException">Thrown if user not found</exception>
        public void ChangeLogin(string login, string newLogin)
        {
            User user = userDataBase.GetUserByLogin(login) as User;
            if (user == null) throw new ArgumentException("User not found");
            string loginRegex = "^[a-zA-Z0-9]*$";
            if (!Regex.IsMatch(newLogin, loginRegex) || userDataBase.ContainsLogin(newLogin)) throw new ArgumentException("Provided login is unavailable");
            user.SetLogin(newLogin);
        }

        /// <summary>
        /// Changes the email of provided user
        /// </summary>
        /// <param name="login">Login of needed user</param>
        /// <param name="newEmail">New email for user</param>
        /// <exception cref="ArgumentException">Thrown if user not found</exception>
        public void ChangeEmail(string login, string newEmail)
        {
            User user = userDataBase.GetUserByLogin(login) as User;
            if (user == null) throw new ArgumentException("User not found");
            string emailRegex = "^\\w+@[a-zA-Z_]+?\\.[a-zA-Z]{2,3}$";
            if (!Regex.IsMatch(newEmail, emailRegex) || userDataBase.ContainsEmail(newEmail)) throw new ArgumentException("Provided email is unavailable");
            user.SetEmail(newEmail);
        }

        /// <summary>
        /// Changes the password of provided user
        /// </summary>
        /// <param name="login">Login of needed user</param>
        /// <param name="newPassword">New password for user</param>
        /// <exception cref="ArgumentException">Thrown if user not found</exception>
        public void ChangePassword(string login, string newPassword)
        {
            User user = userDataBase.GetUserByLogin(login) as User;
            if (user == null) throw new ArgumentException("User not found");
            user.SetPassword(newPassword);
        }

    }
}
