﻿using System;
using Domain.Core;
using Infrastructure.Data.ProductService;

namespace Infrastructure.Business.AdminBusiness
{
    public class AdminProductRepository
    {
        readonly ProductRepository productDataBase;

        public AdminProductRepository(ProductRepository productService)
        {
            productDataBase = productService;
        }

        /// <summary>
        /// Creates new product and adds it to database of products
        /// </summary>
        /// <param name="id">ID for new product</param>
        /// <param name="name">Name for new product</param>
        /// <param name="category">Category for new product</param>
        /// <param name="description">Description for new product</param>
        /// <param name="price">Price for new product</param>
        /// <exception cref="ArgumentException">Thrown if product with provided ID already exist</exception>
        public void CreateProduct(int id, string name, string category, string description, decimal price)
        {
            if (productDataBase.GetProductById(id) != null) throw new ArgumentException("Product with provided ID already exist");
            productDataBase.Add(id, name, category, description, price);
        }

        /// <summary>
        /// Deletes product by its ID and removes it from database of products
        /// </summary>
        /// <param name="id">ID of needed product</param>
        /// <exception cref="ArgumentException">Thrown if product with provided ID does not exist</exception>
        public void DeleteProduct(int id)
        {
            if (productDataBase.GetProductById(id) == null) throw new ArgumentNullException(nameof(id), "Product with provided ID does not exist.");
            productDataBase.Remove(id);
        }

        /// <summary>
        /// Changes name of needed product
        /// </summary>
        /// <param name="id">ID of needed product</param>
        /// <param name="newName">New name for product</param>
        /// <exception cref="ArgumentException">Thrown if product is not found</exception>
        public void ChangeName(int id, string newName)
        {
            Product product = productDataBase.GetProductById(id);
            if (product == null) throw new ArgumentException("Product not found");
            product.SetName(newName);
        }

        /// <summary>
        /// Changes description of needed product
        /// </summary>
        /// <param name="id">ID of needed product</param>
        /// <param name="newDescription">New description for product</param>
        /// <exception cref="ArgumentException">Thrown if product is not found</exception>
        public void ChangeDescription(int id, string newDescription)
        {
            Product product = productDataBase.GetProductById(id);
            if (product == null) throw new ArgumentException("Product not found");
            product.SetDescription(newDescription);
        }

        /// <summary>
        /// Changes category of needed product
        /// </summary>
        /// <param name="id">ID of needed product</param>
        /// <param name="newCategory">New category for product</param>
        /// <exception cref="ArgumentException">Thrown if product is not found</exception>
        public void ChangeCategory(int id, string newCategory)
        {
            Product product = productDataBase.GetProductById(id);
            if (product == null) throw new ArgumentException("Product not found");
            product.SetCategory(newCategory);
        }

        /// <summary>
        /// Changes price of needed product
        /// </summary>
        /// <param name="id">ID of needed product</param>
        /// <param name="newPrice">New price for product</param>
        /// <exception cref="ArgumentException">Thrown if product is not found</exception>
        public void ChangePrice(int id, decimal newPrice)
        {
            Product product = productDataBase.GetProductById(id);
            if (product == null) throw new ArgumentException("Product not found");
            product.SetPrice(newPrice);
        }

    }
}
