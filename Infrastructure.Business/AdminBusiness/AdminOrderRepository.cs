﻿using Domain.Core;

namespace Infrastructure.Business.AdminBusiness
{
    public class AdminOrderRepository
    {
        /// <summary>
        /// Changes the status of order
        /// </summary>
        /// <param name="order">Object of needed order</param>
        /// <param name="status">New status for user</param>
        /// <exception cref="ArgumentException">Thrown if user not found</exception>
        public bool SetStatus(Order order, string status)
        {
            return order.SetStatus(status);
        }

    }
}
