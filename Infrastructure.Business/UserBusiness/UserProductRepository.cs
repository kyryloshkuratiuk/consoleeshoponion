﻿using System.Collections.Generic;
using Domain.Core;
using Infrastructure.Data.ProductService;

namespace Infrastructure.Business.UserBusiness
{
    public class UserProductRepository
    {
        protected ProductRepository productDataBase;

        public UserProductRepository(ProductRepository productService)
        {
            productDataBase = productService;
        }

        /// <summary>
        /// Outputs the list of products with provided name
        /// </summary>
        /// <param name="name">Name of needed product</param>
        public string Find(string name)
        {
            string result = string.Empty;
            List<Product> foundProducts = productDataBase.Find(name);
            if (foundProducts.Count == 0) result = "WARNING: There are no products with such name";
            else foreach (Product product in foundProducts) result += product.ToString() + "\n";
            return result;
        }

        /// <summary>
        /// Outputs the list of products
        /// </summary>
        public string GetProducts()
        {
            string result = string.Empty;
            List<Product> listOfProducts = productDataBase.GetProducts();
            if (listOfProducts.Count == 0) result = "WARNING: There are no products in stock currently";
            else foreach (Product product in listOfProducts) result += product.ToString() + "\n";
            return result;
        }


    }

}
