﻿using System;
using System.Text.RegularExpressions;
using Domain.Core;
using Infrastructure.Data.RegisteredAccountService;

namespace Infrastructure.Business.UserBusiness
{
    public class UserInfoRepository
    {
        protected RegisteredAccountRepository userDataBase;
        protected RegisteredAccount authorizedUser;

        public UserInfoRepository(RegisteredAccount user, RegisteredAccountRepository userService)
        {
            userDataBase = userService;
            authorizedUser = user;
        }

        /// <summary>
        /// Changes the login of current user
        /// </summary>
        /// <param name="newLogin">New login for user</param>
        public void ChangeLogin(string newLogin)
        {
            string loginRegex = "^[a-zA-Z0-9]*$";
            if (!Regex.IsMatch(newLogin, loginRegex) || userDataBase.ContainsLogin(newLogin)) throw new ArgumentException("Provided login is unavailable");
            authorizedUser.SetLogin(newLogin);
        }

        /// <summary>
        /// Changes the email of current user
        /// </summary>
        /// <param name="newEmail">New email for user</param>
        public void ChangeEmail(string newEmail)
        {
            string emailRegex = "^\\w+@[a-zA-Z_]+?\\.[a-zA-Z]{2,3}$";
            if (!Regex.IsMatch(newEmail, emailRegex) || userDataBase.ContainsEmail(newEmail)) throw new ArgumentException("Provided email is unavailable");
            authorizedUser.SetEmail(newEmail);
        }

        /// <summary>
        /// Changes the password of current user
        /// </summary>
        /// <param name="newPassword">New password for user</param>
        public void ChangePassword(string newPassword)
        {
            authorizedUser.SetPassword(newPassword);
        }

        /// <summary>
        /// Adds needed amount to current registered account's balance
        /// </summary>
        /// <param name="amount">Amount to add</param>
        public void Deposit(decimal amount)
        {
            authorizedUser.AddBalance(amount);
        }

    }
}
