﻿using System;
using System.Collections.Generic;
using Domain.Core;
using Infrastructure.Data.OrderService;

namespace Infrastructure.Business.UserBusiness
{
    public class UserOrderRepository
    {
        readonly OrderRepository orderDataBase;
        readonly RegisteredAccount authorizedUser;

        public UserOrderRepository(RegisteredAccount user, OrderRepository orderService)
        {
            orderDataBase = orderService;
            authorizedUser = user;
        }

        /// <summary>
        /// Creates new order and returns its object
        /// </summary>
        public int CreateOrder()
        {
            int order = orderDataBase.Create();
            authorizedUser.AddOrder(orderDataBase.GetOrderById(order));
            return order;
        }

        /// <summary>
        /// Adds provided object of product to the order with provided ID
        /// </summary>
        /// <param name="orderId">ID of needed order</param>
        /// <param name="product">Object of product to add</param>
        public void AddProductToOrder(int orderId, Product product)
        {
            Order order = orderDataBase.GetOrderById(orderId);
            if (product is null) throw new ArgumentException("Product with provided ID does not exist");
            if (order is null) throw new ArgumentException("Order with provided ID does not exist");
            bool result = order.AddProductToOrder(product);
            if (!result) throw new FormatException("Adding products to this order is not possible");
        }

        /// <summary>
        /// Removes provided object of product from the order with provided ID
        /// </summary>
        /// <param name="orderId">ID of needed order</param>
        /// <param name="product">Object of product to add</param>
        public void RemoveProductFromOrder(int orderId, Product product)
        {
            Order order = orderDataBase.GetOrderById(orderId);
            if (product is null) throw new ArgumentException("Product with provided ID does not exist");
            if (order is null) throw new ArgumentException("Order with provided ID does not exist");
            if (order.GetProducts().Count == 0) throw new ArgumentException("Can not remove products from empty order");
            bool result = order.RemoveProductFromOrder(product);
            if (!result) throw new FormatException("Removing products from this order is not possible");
        }

        /// <summary>
        /// Pays for order with provided ID and substracts the value of order from registered user's balance
        /// </summary>
        /// <param name="orderId">ID of needed order</param>
        public bool PayForOrder(int orderId)
        {
            Order currentOrder = orderDataBase.GetOrderById(orderId);
            if (currentOrder is null) throw new ArgumentException("Order with provided ID does not exist");
            if (currentOrder.Status != "New" || currentOrder.Price == 0) throw new FormatException("There is no need to pay for this order");
            if (currentOrder.Status == "New" && currentOrder.Price < authorizedUser.Balance)
            {
                currentOrder.SetStatus("Paid");
                authorizedUser.SubstractBalance(currentOrder.Price);
                return true;
            }
            throw new ArgumentOutOfRangeException(nameof(orderId), "Not enough balance to pay the order");
        }

        /// <summary>
        /// Cancels order with provided ID
        /// </summary>
        /// <param name="orderId">ID of needed order</param>
        public void CancelOrder(int orderId)
        {
            orderDataBase.Delete(orderId);
        }

        /// <summary>
        /// Outputs the history of all registered account's orders
        /// </summary>
        /// <exception cref="NullReferenceException">Thrown if order history is empty</exception>
        public string GetOrdersHistory()
        {
            List<Order> orders = authorizedUser.GetOrders();
            try
            {
                string result = "Orders history: \n";
                foreach (Order order in orders) result += order.ToString() + "\n";
                return result;
            }
            catch (NullReferenceException)
            {
                return "Order history is empty";
            }
        }

        /// <summary>
        /// Changes status of order with provided ID to "Received"
        /// </summary>
        /// <param name="orderId">ID of needed order</param>
        /// <exception cref="ArgumentException">Thrown if order with provided ID does not exist</exception>
        public void SetReceivedStatus(int orderId)
        {
            Order currentOrder = orderDataBase.GetOrderById(orderId);
            if (currentOrder is null) throw new ArgumentException("Order with provided ID does not exist");
            currentOrder.SetReceived();
        }
    }

}
