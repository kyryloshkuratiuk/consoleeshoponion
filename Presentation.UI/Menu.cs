﻿using System;
using Infrastructure.Business;
using Infrastructure.Data;
using Domain.Core;
using Infrastructure.Data.GuestService;
using Infrastructure.Data.RegisteredAccountService;
using Infrastructure.Data.OrderService;
using Infrastructure.Data.ProductService;
using Infrastructure.Business.UserBusiness;
using Infrastructure.Business.AdminBusiness;
using System.Text.RegularExpressions;

namespace Presentation.UI
{
    public class Menu
    {
        RegisteredAccount User;
        RegisteredAccountRepository UserDataBase;
        OrderRepository OrderDataBase;
        ProductRepository ProductDataBase;
        GuestRepository GuestActions; 
        UserProductRepository UserProductAction;
        UserOrderRepository UserOrderAction;
        UserInfoRepository UserInfoAction;
        AdminOrderRepository AdminOrderAction;
        AdminInfoRepository AdminInfoAction;
        AdminProductRepository AdminProductAction;

        string userAction;

        delegate void InterfacePages();
        private InterfacePages page;

        public Menu(RegisteredAccount user, RegisteredAccountRepository userService, OrderRepository OrderRepository, ProductRepository ProductRepository)
        {
            User = user;
            UserDataBase = userService;
            OrderDataBase = OrderRepository;
            ProductDataBase = ProductRepository;
            UserProductAction = new UserProductRepository(ProductDataBase);
            AdminProductAction = new AdminProductRepository(ProductDataBase);
            GuestActions = new GuestRepository();
            page = MainMenu;
        }

        /// <summary>
        /// Starts the user interface
        /// </summary>
        public void Start()
        {
            while (this.page != null)
            {
                this.page.Invoke();
            }
        }

        /// <summary>
        /// Outputs main menu
        /// </summary>
        public void MainMenu()
        {
            if (this.User is User)
            {
                this.page = AuthorizedAccountMenu;
            }
            else if (this.User is Admin)
            {
                this.page = AdminMenu;
            }
            else if (this.User is null)
            {
                this.page = GuestMenu;
            }
        }

        /// <summary>
        /// Outputs guest menu
        /// </summary>
        private void GuestMenu()
        {
            Console.WriteLine("\nPlease, enter the command:\n/getassortment - Show assortment\n/findproduct - Find product(s) by name\n/login - Log in to your account\n" +
                        "/register - Register new account");
            this.userAction = GetUserInput();
            if (userAction.Equals("/getassortment"))
                this.page = GetAssortment;
            if (userAction.Equals("/findproduct"))
                this.page = SearchProduct;
            if (userAction.Equals("/login"))
                this.page = LogIn;
            if (userAction.Equals("/register"))
                this.page = Register;
        }

        /// <summary>
        /// Outputs authorized account menu
        /// </summary>
        public void AuthorizedAccountMenu()
        {
            Console.WriteLine("\nPlease, enter the command:\n/deposit - Deposit\n/getassortment - Show assortment\n/findproduct - Find product(s) by name\n/createorder - Create new order\n" +
                        "/addproduct - Add product to order\n/removeproduct - Remove product from order\n/pay - Pay for order\n/cancel - Cancel order\n" +
                        "/setreceived - Set order status to \"Received\"\n/changepassword - Change password\n/changelogin - Change login\n" +
                        "/changeemail - Change Email\n/gethistory - Show history of orders" +
                        "/getpersonaldata - Show your personal data\n/logout - Log out");
            this.userAction = GetUserInput();
            if (userAction.Equals("/deposit"))
                this.page = Deposit;
            if (userAction.Equals("/getassortment"))
                this.page = GetAssortment;
            if (userAction.Equals("/findproduct"))
                this.page = SearchProduct;
            if (userAction.Equals("/createorder"))
                this.page = CreateOrder;
            if (userAction.Equals("/addproduct"))
                this.page = AddProductToOrder;
            if (userAction.Equals("/removeproduct"))
                this.page = RemoveProductFromOrder;
            if (userAction.Equals("/pay"))
                this.page = PayForOrder;
            if (userAction.Equals("/cancel"))
                this.page = CancelOrder;
            if (userAction.Equals("/setreceived"))
                this.page = ReceiveOrder;
            if (userAction.Equals("/changepassword"))
                this.page = ChangePassword;
            if (userAction.Equals("/changelogin"))
                this.page = ChangeLogin;
            if (userAction.Equals("/changeemail"))
                this.page = ChangeEmail;
            if (userAction.Equals("/gethistory"))
                this.page = GetHistory;
            if (userAction.Equals("/getpersonaldata"))
                this.page = GetPersonalData;
            else if (userAction.Equals("/logout") && this.User != null)
                this.page = LogOut;
        }

        /// <summary>
        /// Outputs admin menu
        /// </summary>
        public void AdminMenu()
        {
            Console.WriteLine("\nPlease, enter the command:\nADMIN COMMANDS:\n/getuserdata - Show personal data of specific user\n/changeuserlogin - Change login of specific user\n" +
                        "/changeuserpassword - Change password of specific user\n/changeuseremail - Change email of specific user\n/createproduct - Create new product\n" +
                        "/changename - Edit product name\n/changecategory - Edit product category\n/changedescription - Edit product description\n" +
                        "/changeprice - Edit product price\n/changeorderstatus - Change order status\n\nUSER COMMANDS\n/deposit - Deposit\n" +
                        "/getassortment - Show assortment\n/findproduct - Find product(s) by name\n/createorder - Create new order\n" +
                        "/addproduct - Add product to order\n/removeproduct - Remove product from order\n/pay - Pay for order\n/cancel - Cancel order\n" +
                        "/setreceived - Set order status to \"Received\"\n/changepassword - Change password\n/changelogin - Change login\n" +
                        "/changeemail - Change Email\n/gethistory - Show history of orders" +
                        "/getpersonaldata - Show your personal data\n/logout - Log out");
            this.userAction = GetUserInput();
            if (userAction.Equals("/getuserdata"))
                this.page = GetUserData;
            if (userAction.Equals("/changeuserlogin"))
                this.page = ChangeUserLogin;
            if (userAction.Equals("/changeuserpassword"))
                this.page = ChangeUserPassword;
            if (userAction.Equals("/changeuseremail"))
                this.page = ChangeUserEmail;
            if (userAction.Equals("/createproduct"))
                this.page = CreateProduct;
            if (userAction.Equals("/changename"))
                this.page = ChangeName;
            if (userAction.Equals("/changecategory"))
                this.page = ChangeCategory;
            if (userAction.Equals("/changedescription"))
                this.page = ChangeDescription;
            if (userAction.Equals("/changeprice"))
                this.page = ChangePrice;
            if (userAction.Equals("/changeorderstatus"))
                this.page = ChangeOrderStatus;
            if (userAction.Equals("/deposit"))
                this.page = Deposit;
            if (userAction.Equals("/getassortment"))
                this.page = GetAssortment;
            if (userAction.Equals("/findproduct"))
                this.page = SearchProduct;
            if (userAction.Equals("/createorder"))
                this.page = CreateOrder;
            if (userAction.Equals("/addproduct"))
                this.page = AddProductToOrder;
            if (userAction.Equals("/removeproduct"))
                this.page = RemoveProductFromOrder;
            if (userAction.Equals("/pay"))
                this.page = PayForOrder;
            if (userAction.Equals("/cancel"))
                this.page = CancelOrder;
            if (userAction.Equals("/setreceived"))
                this.page = ReceiveOrder;
            if (userAction.Equals("/changepassword"))
                this.page = ChangePassword;
            if (userAction.Equals("/changelogin"))
                this.page = ChangeLogin;
            if (userAction.Equals("/changeemail"))
                this.page = ChangeEmail;
            if (userAction.Equals("/gethistory"))
                this.page = GetHistory;
            if (userAction.Equals("/getpersonaldata"))
                this.page = GetPersonalData;
            else if (userAction.Equals("/logout") && this.User != null)
                this.page = LogOut;
        }

        /// <summary>
        /// Registers new user
        /// </summary>
        private void Register()
        {
            string email = string.Empty, login = "", password = string.Empty;
            while (!Regex.IsMatch(email, "^\\w+@[a-zA-Z_]+?\\.[a-zA-Z]{2,3}$"))
            {
                Console.WriteLine("Please, enter the email using such template \"mymail@mail.com\":");
                email = GetUserInput();
            }
            while (!Regex.IsMatch(login, "^[a-z0-9_-]{3,16}$"))
            {
                Console.WriteLine("Please, enter the login using latin letters and/or numbers:");
                login = GetUserInput();
            }
            while (password.Length < 6)
            {
                Console.WriteLine("Please, enter the password with length more than 6 symbols:");
                password = GetUserInput();
            }
            try
            {
                this.Clear();
                GuestActions.Register(login, password, email, UserDataBase);
                User = new User(login, password, email);
                UserOrderAction = new UserOrderRepository(User, OrderDataBase);
                UserInfoAction = new UserInfoRepository(User, UserDataBase);
                this.page = MainMenu;
            }
            catch (Exception e)
            {
                this.Clear();
                Console.WriteLine("ERROR: " + e.Message);
                this.page = MainMenu;
            }
        }

        /// <summary>
        /// Authorizes registered user
        /// </summary>
        private void LogIn()
        {
            Console.WriteLine("Please, enter the login:");
            string login = GetUserInput();
            Console.WriteLine("Please, enter password:");
            string password = GetUserInput();
            try
            {
                this.Clear();
                User = GuestActions.Authorize(login, password, UserDataBase);
                UserOrderAction = new UserOrderRepository(User, OrderDataBase);
                UserInfoAction = new UserInfoRepository(User, UserDataBase);
                AdminOrderAction = new AdminOrderRepository();
                AdminInfoAction = new AdminInfoRepository(UserDataBase);
                this.page = MainMenu;
            }
            catch (Exception e)
            {
                this.Clear();
                Console.WriteLine("ERROR: " + e.Message);
                this.page = MainMenu;
            }
        }

        /// <summary>
        /// Outputs assortment of products
        /// </summary>
        public void GetAssortment()
        {
            this.Clear();
            Console.WriteLine(UserProductAction.GetProducts());
            this.page = MainMenu;
        }

        /// <summary>
        /// Searches specific product
        /// </summary>
        public void SearchProduct()
        {
            Console.WriteLine("Enter the product name");
            string name = this.GetUserInput();
            this.Clear();
            Console.WriteLine(UserProductAction.Find(name));
            this.page = MainMenu;
        }

        /// <summary>
        /// Outputs current registered account's history of orders
        /// </summary>
        public void GetHistory()
        {
            this.Clear();
            Console.WriteLine(UserOrderAction.GetOrdersHistory());
            this.page = MainMenu;
        }

        /// <summary>
        /// Outputs current registered account's personal data
        /// </summary>
        public void GetPersonalData()
        {
            this.Clear();
            Console.WriteLine(User);
            this.page = MainMenu;
        }

        /// <summary>
        /// Outputs provided registered account's personal data
        /// </summary>
        public void GetUserData()
        {
            this.Clear();
            Console.WriteLine("Enter the login of user:");
            string userLogin = this.GetUserInput();
            try
            {
                Console.WriteLine(AdminInfoAction.GetPersonalData(userLogin));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            this.page = MainMenu;
        }

        /// <summary>
        /// Changes provided registered account's password
        /// </summary>
        public void ChangeUserPassword()
        {
            this.Clear();
            Console.WriteLine("Enter user login:");
            string login = this.GetUserInput();
            Console.WriteLine("Enter new password:");
            string password = this.GetUserInput();
            try
            {
                AdminInfoAction.ChangePassword(login, password);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            this.page = MainMenu;
        }

        /// <summary>
        /// Changes provided registered account's login
        /// </summary>
        public void ChangeUserLogin()
        {
            this.Clear();
            Console.WriteLine("Enter user login:");
            string login = this.GetUserInput();
            Console.WriteLine("Enter new login:");
            string newLogin = this.GetUserInput();
            try
            {
                AdminInfoAction.ChangeLogin(login, newLogin);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            this.page = MainMenu;
        }

        /// <summary>
        /// Changes provided registered account's email
        /// </summary>
        public void ChangeUserEmail()
        {
            this.Clear();
            Console.WriteLine("Enter user login:");
            string login = this.GetUserInput();
            Console.WriteLine("Enter new email:");
            string email = this.GetUserInput();
            try
            {
                AdminInfoAction.ChangeEmail(login, email);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            this.page = MainMenu;
        }

        /// <summary>
        /// Changes current registered account's password
        /// </summary>
        public void ChangePassword()
        {
            this.Clear();
            Console.WriteLine("Enter new password:");
            string password = this.GetUserInput();
            try
            {
                UserInfoAction.ChangePassword(password);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            this.page = MainMenu;
        }

        /// <summary>
        /// Changes current registered account's login
        /// </summary>
        public void ChangeLogin()
        {
            this.Clear();
            Console.WriteLine("Enter new login:");
            string login = this.GetUserInput();
            try
            {
                UserInfoAction.ChangeLogin(login);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            this.page = MainMenu;
        }

        /// <summary>
        /// Changes current registered account's email
        /// </summary>
        public void ChangeEmail()
        {
            this.Clear();
            Console.WriteLine("Enter new email:");
            string email = this.GetUserInput();
            try
            {
                UserInfoAction.ChangeEmail(email);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            this.page = MainMenu;
        }

        /// <summary>
        /// Creates new product
        /// </summary>
        public void CreateProduct()
        {
            this.Clear();
            Console.WriteLine("Enter product Id");
            string strId = this.GetUserInput();
            Console.WriteLine("Enter product name");
            string strName = this.GetUserInput();
            Console.WriteLine("Enter product category");
            string strCategory = this.GetUserInput();
            Console.WriteLine("Enter product description");
            string strDescription = this.GetUserInput();
            Console.WriteLine("Enter product price");
            string strPrice = this.GetUserInput();
            try
            {
                int id = Int32.Parse(strId);
                decimal price = Decimal.Parse(strPrice);
                AdminProductAction.CreateProduct(id, strName, strCategory, strDescription, price);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            this.page = MainMenu;
        }

        /// <summary>
        /// Changes product's name
        /// </summary>
        public void ChangeName()
        {
            this.Clear();
            Console.WriteLine("Enter product id");
            string strId = this.GetUserInput();
            Console.WriteLine("Enter new name");
            string newName = this.GetUserInput();
            try
            {
                int id = Int32.Parse(strId);
                AdminProductAction.ChangeName(id, newName);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            this.page = MainMenu;
        }

        /// <summary>
        /// Changes product's category
        /// </summary>
        public void ChangeCategory()
        {
            this.Clear();
            Console.WriteLine("Enter product id");
            string strId = this.GetUserInput();
            Console.WriteLine("Enter new category");
            string newName = this.GetUserInput();
            try
            {
                int id = Int32.Parse(strId);
                AdminProductAction.ChangeCategory(id, newName);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            this.page = MainMenu;
        }

        /// <summary>
        /// Changes product's description
        /// </summary>
        public void ChangeDescription()
        {
            this.Clear();
            Console.WriteLine("Enter product id");
            string strId = this.GetUserInput();
            Console.WriteLine("Enter new description");
            string newName = this.GetUserInput();
            try
            {
                int id = Int32.Parse(strId);
                AdminProductAction.ChangeDescription(id, newName);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            this.page = MainMenu;
        }

        /// <summary>
        /// Changes product's price
        /// </summary>
        public void ChangePrice()
        {
            this.Clear();
            Console.WriteLine("Enter product id");
            string strId = this.GetUserInput();
            Console.WriteLine("Enter new price");
            string newStrPrice = this.GetUserInput();
            try
            {
                int id = Int32.Parse(strId);
                decimal newPrice = Decimal.Parse(newStrPrice);
                AdminProductAction.ChangePrice(id, newPrice);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            this.page = MainMenu;
        }

        /// <summary>
        /// Creates new order
        /// </summary>
        public void CreateOrder()
        {
            this.Clear();
            Console.WriteLine("Order with ID: " + UserOrderAction.CreateOrder() + " created successfully");
            this.page = MainMenu;
        }

        /// <summary>
        /// Changes provided order's status
        /// </summary>
        public void ChangeOrderStatus()
        {
            this.Clear();
            Console.WriteLine("Enter order id");
            string strOrderId = this.GetUserInput();
            Console.WriteLine("Enter new status (Canceled by administrator, Paid, Sent, Received, Finished)");
            string newStatus = this.GetUserInput();
            try
            {
                int orderId = Int32.Parse(strOrderId);
                AdminOrderAction.SetStatus(OrderDataBase.GetOrderById(orderId), newStatus);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            this.page = MainMenu;
        }

        /// <summary>
        /// Cancels order with provided ID
        /// </summary>
        public void CancelOrder()
        {
            this.Clear();
            Console.WriteLine("Please, enter the order ID");
            string strOrderId = this.GetUserInput();
            try
            {
                int orderId = Int32.Parse(strOrderId);
                UserOrderAction.CancelOrder(orderId);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            this.page = MainMenu;
        }

        /// <summary>
        /// Changes status of provided order to "Received"
        /// </summary>
        public void ReceiveOrder()
        {
            this.Clear();
            Console.WriteLine("Please, enter the order ID");
            string strOrderId = this.GetUserInput();
            try
            {
                int orderId = Int32.Parse(strOrderId);
                UserOrderAction.SetReceivedStatus(orderId);
            }
            catch
            {
                Console.WriteLine("Provided ID is invalid");
                this.page = MainMenu;
            }
            this.page = MainMenu;
        }

        /// <summary>
        /// Adds provided product to provided order
        /// </summary>
        public void AddProductToOrder()
        {
            Console.WriteLine("Please, enter the order ID");
            string strOrderId = this.GetUserInput();
            Console.WriteLine("Please, enter the product ID");
            string strProductId = this.GetUserInput();
            this.Clear();
            try
            {
                int orderId = Int32.Parse(strOrderId);
                int productId = Int32.Parse(strProductId);
                UserOrderAction.AddProductToOrder(orderId, ProductDataBase.GetProductById(productId));
            }
            catch
            {
                Console.WriteLine("Provided ID is invalid");
            }
            this.page = MainMenu;
        }

        /// <summary>
        /// Removes provided product from provided order
        /// </summary>
        public void RemoveProductFromOrder()
        {
            Console.WriteLine("Please, enter the order ID");
            string strOrderId = this.GetUserInput();
            Console.WriteLine("Please, enter the product ID");
            string strProductId = this.GetUserInput();
            this.Clear();
            try
            {
                int orderId = Int32.Parse(strOrderId);
                int productId = Int32.Parse(strProductId);
                UserOrderAction.RemoveProductFromOrder(orderId, ProductDataBase.GetProductById(productId));
            }
            catch
            {
                Console.WriteLine("Provided ID is invalid");
            }
            this.page = MainMenu;
        }

        /// <summary>
        /// Adds balance to registered account's balance
        /// </summary>
        public void Deposit()
        {
            Console.WriteLine("Enter the amount you want to deposit");
            string strDepositAmount = this.GetUserInput();
            this.Clear();
            try
            {
                decimal depositAmount = Decimal.Parse(strDepositAmount);
                UserInfoAction.Deposit(depositAmount);
                Console.WriteLine("Money added to your balance successfully");
            }
            catch
            {
                Console.WriteLine("Deposit failed. Please, try again");
            }
            this.page = MainMenu;
        }

        /// <summary>
        /// Pays for registered account's order
        /// </summary>
        public void PayForOrder()
        {
            Console.WriteLine("Please, enter the order ID");
            string strOrderId = this.GetUserInput();
            this.Clear();
            try
            {
                int orderId = Int32.Parse(strOrderId);
                UserOrderAction.PayForOrder(orderId);
            }
            catch (FormatException)
            {
                Console.WriteLine("There is no need to pay for this order");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            this.page = MainMenu;
        }

        /// <summary>
        /// Logs out registered account
        /// </summary>
        public void LogOut()
        {
            this.Clear();
            User = null;
            this.page = MainMenu;
        }

        /// <summary>
        /// Gets the input of current user
        /// </summary>
        public string GetUserInput()
        {
            return Console.ReadLine();
        }

        /// <summary>
        /// Clears the console
        /// </summary>
        public void Clear()
        {
            Console.Clear();
        }

    }
}
