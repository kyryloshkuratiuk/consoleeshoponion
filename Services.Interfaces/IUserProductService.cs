﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Interfaces
{
    interface IUserProductService
    {
        public string Find(string name);
        public string GetProducts();
    }
}
