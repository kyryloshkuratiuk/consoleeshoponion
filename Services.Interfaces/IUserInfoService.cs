﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Interfaces
{
    public interface IUserInfoService
    {
        public void ChangeLogin(string newLogin);
        public void ChangeEmail(string newEmail);
        public void ChangePassword(string newPassword);
        public void Deposit(decimal amount);
    }
}
