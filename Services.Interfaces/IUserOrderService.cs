﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Core;

namespace Services.Interfaces
{
    public interface IUserOrderService
    {
        public int CreateOrder();
        public void AddProductToOrder(int orderId, Product product);
        public void RemoveProductFromOrder(int orderId, Product product);
        public bool PayForOrder(int orderId);
        public void CancelOrder(int orderId);
        public string GetOrdersHistory();
        public void SetReceivedStatus(int orderId);
    }
}
