﻿using System;
using System.Collections.Generic;
using Domain.Core;

namespace Domain.Interfaces
{
    public interface IRegisteredAccountRepository
    {
        /// <summary>
        /// Adds an object of registered account to the database of registered accounts
        /// </summary>
        /// <param name="user">Object of registeredaccount</param>
        public void Add(RegisteredAccount user);

        /// <summary>
        /// Removes an object of registered account from the database of registered accounts
        /// </summary>
        /// <param name="user">Object of registeredaccount</param>
        public void Remove(RegisteredAccount user);

        /// <summary>
        /// Checks if registered account database contains user with provided login and password
        /// </summary>
        /// <param name="login">Needed login</param>
        /// <param name="password">Needed password</param>
        public bool ContainsUser(string login, string password);

        /// <summary>
        /// Checks if registered account database contains user with provided email
        /// </summary>
        /// <param name="email">Needed email</param>
        public bool ContainsEmail(string email);

        /// <summary>
        /// Checks if registered account database contains user with provided email
        /// </summary>
        /// <param name="login">Needed email</param>
        public bool ContainsLogin(string login);

        /// <summary>
        /// Gets object of registered accont with provided login and password
        /// </summary>
        /// <param name="login">Needed login</param>
        /// <param name="password">Needed password</param>
        public RegisteredAccount GetUser(string login, string password);

        /// <summary>
        /// Gets object of registered accont with provided login
        /// </summary>
        /// <param name="login">Needed login</param>
        public RegisteredAccount GetUserByLogin(string login);

        /// <summary>
        /// Returns list of all registered accounts
        /// </summary>
        public List<RegisteredAccount> GetUsers();
    }
}
