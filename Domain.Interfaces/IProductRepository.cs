﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Core;

namespace Domain.Interfaces
{
    public interface IProductRepository
    {
        /// <summary>
        /// Adds new product to database of products
        /// </summary>
        /// <param name="id">ID for new product</param>
        /// <param name="name">Name for new product</param>
        /// <param name="category">Category for new product</param>
        /// <param name="description">Description for new product</param>
        /// <param name="price">Price for new product</param>
        public void Add(int id, string name, string category, string description, decimal price);

        /// <summary>
        /// Removes product from database of products by ID
        /// </summary>
        /// <param name="id">ID of product</param>
        public void Remove(int id);

        /// <summary>
        /// Returns product by ID
        /// </summary>
        /// <param name="id">ID of product</param>
        public Product GetProductById(int id);

        /// <summary>
        /// Returns list of products that include provided name
        /// </summary>
        /// <param name="productName">Name of needed product</param>
        public List<Product> Find(string productName);

        /// <summary>
        /// Returns list of products
        /// </summary>
        public List<Product> GetProducts();
    }
}
