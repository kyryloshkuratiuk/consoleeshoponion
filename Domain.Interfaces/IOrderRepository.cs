﻿using System;
using Domain.Core;

namespace Domain.Interfaces
{
    public interface IOrderRepository
    {
        /// <summary>
        /// Creates new order and returns its ID
        /// </summary>
        public int Create();

        /// <summary>
        /// Sets order status to removed
        /// </summary>
        /// <param name="orderId">ID of needed order</param>
        public void Delete(int orderId);

        /// <summary>
        /// Returns object of the order by ID
        /// </summary>
        /// <param name="id">ID of needed order</param>
        public Order GetOrderById(int id);
    }
}
