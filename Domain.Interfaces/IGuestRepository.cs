﻿using System;
using Domain.Core;
namespace Domain.Interfaces
{
    public interface IGuestRepository
    {
        /// <summary>
        /// Registers new user and adds it to database of users
        /// </summary>
        /// <param name="login">Login for new user</param>
        /// <param name="password">Password for new user</param>
        /// <param name="email">Email for new user</param>
        /// <param name="userService">Object of service of registered accounts</param>
        public bool Register(string login, string password, string email, IRegisteredAccountRepository userService);
        
        /// <summary>
        /// Authorizes registered user and returns its object
        /// </summary>
        /// <param name="login">Login of user</param>
        /// <param name="password">Password of user</param>
        /// <param name="userService">Object of service of registered accounts</param>
        public RegisteredAccount Authorize(string login, string password, IRegisteredAccountRepository userService);
    }
}
