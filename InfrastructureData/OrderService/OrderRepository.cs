﻿using System;
using System.Collections.Generic;
using Domain.Core;
using Domain.Interfaces;

namespace Infrastructure.Data.OrderService
{
    public class OrderRepository : IOrderRepository
    {
        private OrderContext orderDB = new OrderContext();

        /// <summary>
        /// Creates new order and returns its ID
        /// </summary>
        public int Create()
        {
            int id = orderDB.orders.Count;
            List<Product> products = new List<Product>();
            orderDB.orders.Add(new Order(id, products));
            return id;
        }

        /// <summary>
        /// Sets order status to removed
        /// </summary>
        /// <param name="orderId">ID of needed order</param>
        public void Delete(int orderId)
        {
            foreach (Order order in orderDB.orders)
            {
                if (order == GetOrderById(orderId))
                {
                    if (order.Status == "Received" || order.Status == "Finished" || order.Status == "Canceled by user") throw new ArgumentException("This order can not be canceled");
                    order.Cancel();
                }
            }
        }

        /// <summary>
        /// Returns object of the order by ID
        /// </summary>
        /// <param name="id">ID of needed order</param>
        public Order GetOrderById(int id)
        {
            foreach (Order order in orderDB.orders)
            {
                if (order.Id == id) return order;
            }
            return null;
        }

    }
}
