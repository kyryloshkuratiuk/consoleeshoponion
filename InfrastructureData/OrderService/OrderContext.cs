﻿using System.Collections.Generic;
using Domain.Core;

namespace Infrastructure.Data.OrderService
{
    class OrderContext : DataContext<Order>
    {
        public List<Order> orders = new List<Order>();
    }
}
