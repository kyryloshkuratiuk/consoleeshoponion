﻿using System.Collections.Generic;
using Domain.Core;

namespace Infrastructure.Data.ProductService
{
    class ProductContext : DataContext<Product>
    {
        public List<Product> products = new List<Product>();
    }
}
