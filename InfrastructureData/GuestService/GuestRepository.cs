﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using Domain.Core;
using Domain.Interfaces;
namespace Infrastructure.Data.GuestService
{
    public class GuestRepository : IGuestRepository
    {
        /// <summary>
        /// Registers new user and adds it to database of users
        /// </summary>
        /// <param name="login">Login for new user</param>
        /// <param name="password">Password for new user</param>
        /// <param name="email">Email for new user</param>
        /// <param name="userService">Object of service of registered accounts</param>
        /// <exception cref="ArgumentException">Thrown if login or email is written incorrectly or if login or email already exists</exception>
        public bool Register(string login, string password, string email, IRegisteredAccountRepository userService)
        {
            string loginRegex = "^[a-z0-9_-]{3,16}$";
            string emailRegex = "^\\w+@[a-zA-Z_]+?\\.[a-zA-Z]{2,3}$";

            if (!Regex.IsMatch(login, loginRegex) || !Regex.IsMatch(email, emailRegex)) throw new ArgumentException("Provided login or email is wrong");
            if (userService.ContainsEmail(email)) throw new ArgumentException("User with provided email already exists.");
            if (userService.ContainsLogin(login)) throw new ArgumentException("User with provided login already exists.");

            userService.Add(new User(login, password, email));
            return true;
        }

        /// <summary>
        /// Authorizes registered user and returns its object
        /// </summary>
        /// <param name="login">Login of user</param>
        /// <param name="password">Password of user</param>
        /// <param name="userService">Object of service of registered accounts</param>
        public RegisteredAccount Authorize(string login, string password, IRegisteredAccountRepository userService)
        {
            return userService.GetUser(login, password);
        }

    }
}
