﻿using System;
using System.Collections.Generic;
using Domain.Core;
using Domain.Interfaces;

namespace Infrastructure.Data.RegisteredAccountService
{
    public class RegisteredAccountRepository : IRegisteredAccountRepository
    {
        private RegisteredAccountContext registeredAccountDB = new RegisteredAccountContext();

        /// <summary>
        /// Adds an object of registered account to the database of registered accounts
        /// </summary>
        /// <param name="user">Object of registeredaccount</param>
        public void Add(RegisteredAccount user) { registeredAccountDB.users.Add(user); }

        /// <summary>
        /// Removes an object of registered account from the database of registered accounts
        /// </summary>
        /// <param name="user">Object of registeredaccount</param>
        public void Remove(RegisteredAccount user) { registeredAccountDB.users.Remove(user); }

        /// <summary>
        /// Checks if registered account database contains user with provided login and password
        /// </summary>
        /// <param name="login">Needed login</param>
        /// <param name="password">Needed password</param>
        public bool ContainsUser(string login, string password)
        {
            foreach (RegisteredAccount user in registeredAccountDB.users)
            {
                if (user.Login == login && user.Password == password) return true;
            }
            return false;
        }

        /// <summary>
        /// Checks if registered account database contains user with provided email
        /// </summary>
        /// <param name="email">Needed email</param>
        public bool ContainsEmail(string email)
        {
            foreach (RegisteredAccount user in registeredAccountDB.users)
            {
                if (user.Email == email) return true;
            }
            return false;
        }

        /// <summary>
        /// Checks if registered account database contains user with provided email
        /// </summary>
        /// <param name="login">Needed email</param>
        public bool ContainsLogin(string login)
        {
            foreach (RegisteredAccount user in registeredAccountDB.users)
            {
                if (user.Login == login) return true;
            }
            return false;
        }

        /// <summary>
        /// Gets object of registered accont with provided login and password
        /// </summary>
        /// <param name="login">Needed login</param>
        /// <param name="password">Needed password</param>
        public RegisteredAccount GetUser(string login, string password)
        {
            foreach (RegisteredAccount user in registeredAccountDB.users)
            {
                if (user.Login == login && user.Password == password) return user;
            }
            throw new ArgumentException("User not found");
        }

        /// <summary>
        /// Gets object of registered accont with provided login
        /// </summary>
        /// <param name="login">Needed login</param>
        public RegisteredAccount GetUserByLogin(string login)
        {
            foreach (RegisteredAccount user in registeredAccountDB.users)
            {
                if (user.Login == login) return user;
            }
            return null;
        }

        /// <summary>
        /// Returns list of all registered accounts
        /// </summary>
        public List<RegisteredAccount> GetUsers()
        {
            return registeredAccountDB.users;
        }
    }

}
