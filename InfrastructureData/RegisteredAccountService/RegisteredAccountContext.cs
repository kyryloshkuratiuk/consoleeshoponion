﻿using System.Collections.Generic;
using Domain.Core;

namespace Infrastructure.Data.RegisteredAccountService
{
    class RegisteredAccountContext : DataContext<RegisteredAccount>
    {
        public List<RegisteredAccount> users = new List<RegisteredAccount>();
    }
}
