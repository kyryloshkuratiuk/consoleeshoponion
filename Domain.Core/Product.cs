﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Core
{
    public class Product
    {
        public int Id { get; private set; }
        public string Name { get; private set; }
        public string Category { get; private set; }
        public string Description { get; private set; }
        public decimal Price { get; private set; }

        public Product(int id, string name, string category, string description, decimal price)
        {
            Id = id;
            Name = name;
            Category = category;
            Description = description;
            Price = price;
        }

        /// <summary>
        /// Changes name of current product
        /// </summary>
        /// <param name="name">New name of product</param>
        public void SetName(string name)
        {
            Name = name;
        }

        /// <summary>
        /// Changes category of current product
        /// </summary>
        /// <param name="category">New category of product</param>
        public void SetCategory(string category)
        {
            Category = category;
        }

        /// <summary>
        /// Changes description of current product
        /// </summary>
        /// <param name="description">New description of product</param>
        public void SetDescription(string description)
        {
            Description = description;
        }

        /// <summary>
        /// Changes price of current product
        /// </summary>
        /// <param name="price">New price of product</param>
        public void SetPrice(decimal price)
        {
            Price = price;
        }

        /// <summary>
        /// Returns product as a string
        /// </summary>
        public override string ToString()
        {
            return "ID " + Id + " | " + Category + " | " + Name + " | " + Description + " | " + Price + " UAH";
        }
    }

}
