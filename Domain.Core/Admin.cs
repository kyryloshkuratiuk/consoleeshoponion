﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Core
{
    public class Admin : RegisteredAccount
    {
        public Admin(string login, string password, string email) : base(login, password, email, EntitieRole.Admin) { }
    }
}
