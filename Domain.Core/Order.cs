﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Core
{
    public class Order
    {
        public int Id { get; private set; }
        public string Status { get; private set; }
        public decimal Price { get; set; }
        public List<Product> Products { get; set; }

        public Order(int id, List<Product> products)
        {
            Id = id;
            Status = "New";
            Products = products;
            Price = 0;
        }

        /// <summary>
        /// Changes status of order to "Received"
        /// </summary>
        public void SetReceived()
        {
            Status = "Received";
        }

        /// <summary>
        /// Changes status of order to "Canceled by user"
        /// </summary>
        public void Cancel()
        {
            Status = "Canceled by user";
        }

        /// <summary>
        /// Changes status of order to provided
        /// </summary>
        /// <param name="status">New status for order</param>
        public bool SetStatus(string status)
        {
            if ((Status == "New" || Status == "Canceled by administrator" || Status == "Sent" || Status == "Received" || Status == "Finished") && (status == "Canceled by administrator" || status == "Paid" || status == "Sent" || status == "Received" || status == "Finished"))
            {
                Status = status;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Adds new product to order
        /// </summary>
        /// <param name="product">Object of needed product</param>
        public bool AddProductToOrder(Product product)
        {
            if (Status != "New") return false;
            Products.Add(product);
            Price += product.Price;
            return true;
        }

        /// <summary>
        /// Removes product from order
        /// </summary>
        /// <param name="product">Object of needed product</param>
        public bool RemoveProductFromOrder(Product product)
        {
            if (Status != "New") return false;
            Products.Remove(product);
            Price -= product.Price;
            return true;
        }

        /// <summary>
        /// Returns list of all products
        /// </summary>
        public List<Product> GetProducts()
        {
            return Products;
        }

        /// <summary>
        /// Returns order as a string
        /// </summary>
        public override string ToString()
        {
            string result = "Order ID: " + Id + " | Status: " + Status + " | Price: " + Price + "\n";
            foreach (Product product in Products) result += product.ToString() + "\n";
            return result;
        }
    }

}
