﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Core
{
    /// <summary>
    /// Enumeration registered account's available roles
    /// </summary>
    public enum EntitieRole
    {
        User,
        Admin
    }

}
