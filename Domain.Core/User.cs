﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Core
{
    public class User : RegisteredAccount
    {
        public User(string login, string password, string email) : base(login, password, email, EntitieRole.User) { }
    }

}
